# -*- coding: utf-8 -*-
import os
import django

#  you have to set the correct path to you settings module
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "courier_tracking.settings")
django.setup()

import urllib2, json
from tracking.models import Courier

response = urllib2.urlopen('https://shipway.in/api/carriers')
json_data = response.read()
#print json_data
data = json.loads(json_data)
print data
for i in data["couriers"]:
    c = Courier(courier_name = i["courier_name"], image=i["image"], id=int(i["id"]))
    c.save()

