# -*- coding: utf-8 -*-
import urllib2, json
import os
import django
import requests
import traceback
from dateutil import parser

from Queue import Queue
from threading import Thread


#  you have to set the correct path to you settings module
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "courier_tracking.settings")
django.setup()

from tracking.models import Courier, Parcel, TrackingDetails, add_to_shipway

count_not_added_shipway = Parcel.objects.filter(AddedToShipway = False).count()

def do_stuff(q):
  while True:
    p = q.get()
    add_to_shipway(p)
    q.task_done()

if count_not_added_shipway>0:
    q = Queue(maxsize=0)
    num_threads = min(20, count_not_added_shipway)

    for i in range(num_threads):
        worker = Thread(target=do_stuff, args=(q,))
        worker.setDaemon(True)
        worker.start()

    for p in Parcel.objects.filter(AddedToShipway = False):
        q.put(p)

    q.join()

for id in Parcel.objects.filter(Delivered = False, AddedToShipway=True).values_list("id", flat = True):
    try:
        data = {"username":"sarthakjain",\
        "password":"d0ef2a757e6e0c05e6cfaec766bbfd45",\
        "order_id":str(id), \
        }

        data = json.dumps(data)

        url = "https://shipway.in/api/getOrderShipmentDetails"

        headers = {
            'content-type': "application/json",
        }

        response = requests.request("POST", url, data=data, headers=headers)

        json_reponse = response.text

        response = json.loads(json_reponse)
        if response["status"] == "Success":
            response = response["response"]

            p = Parcel.objects.get(id = id)

            t = TrackingDetails(Parcel = p)
            t.Courier = p.Courier
            t.Status = response["current_status"]
            t.StatusCode = response["current_status_code"]
                
            if "awbno" in response:
                t.WaybillNumber = response["awbno"]
                if len(response["pickupdate"])>0:
                    t.PickupDate = parser.parse(response["pickupdate"])
                t.Recepient = response["recipient"]
                t.From = response["from"]
                t.To = response["to"]
                if len(response["time"])>0:
                    t.DeliveryTime = parser.parse(response["time"])
                t.TrackingUrl = response["tracking_url"]
            t.save()

            p.Status = response["current_status"]
            p.StatusCode = response["current_status_code"]
            
            if "awbno" in response:
                p.WaybillNumber = response["awbno"]
                if len(response["pickupdate"])>0:
                    p.PickupDate = parser.parse(response["pickupdate"])
                p.Recepient = response["recipient"]
                p.From = response["from"]
                p.To = response["to"]
                if len(response["time"])>0:
                    p.DeliveryTime = parser.parse(response["time"])
                p.TrackingUrl = response["tracking_url"]
                if t.StatusCode == "DEL":
                    p.Delivered = True
            p.save()
    except:
        traceback.print_exc()