# README #

### What is this repository for? ###

* The project is used to track shipments using the shipway api. Available at shipway.in. It stores details and tracking numbers

### How do I get set up? ###

* clone the repository using "git clone https://bitbucket.org/lifespanclinics/courier_tracking"
* go inside the folder "cd courier_tracking"
* to install dependencies run "pip install -r requirements.txt"
* to create the database and run migrations run "python manage.py migrate"
* to import all the couriers into the db run "python fetch_couriers.py"
* to create super user run "python manage.py createsuperuser"
* to add a cron job to fetch tracking updates run 'echo "0 \* \* \* \* python /home/ubuntu/courier_tracking/get_shipment_details.py" | tee -a /var/spool/cron/root'

### Contribution guidelines ###

* To add changes create a pull request to the master brach

### Who do I talk to? ###

* sarthak@lifespanindia.com