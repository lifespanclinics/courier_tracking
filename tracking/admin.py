# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Parcel, Courier, TrackingDetails, ExcelFile
from django.http import HttpResponse
import csv
# Register your models here.

class TrackingDetailsInline(admin.TabularInline):
    model = TrackingDetails
    readonly_fields = ["Courier", "WaybillNumber", "PickupDate", "Status", "StatusCode", "Recepient", "From", "To", "DeliveryTime", "TrackingUrl"]

class ParcelAdmin(admin.ModelAdmin):
    actions = ['download_csv']
    list_display = [field.name for field in Parcel._meta.fields if field.name != "id"]
    inlines = [TrackingDetailsInline]
    search_fields = ('WaybillNumber',)
    list_filter = ("Delivered", "PickupDate", "Status", "StatusCode", "DeliveryTime", "AddedToShipway")
    readonly_fields = ["Delivered", "PickupDate", "Status", "StatusCode", "TrackingUrl", "DeliveryTime", "AddedToShipway"]

    def download_csv(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="courier.csv"'
        writer = csv.writer(response)

        writer.writerow(["CourierTag", "Consignee_Code","Consignee_Name", "C Note Number", "Courier Date", "Courier Name", "Status", "Add_1","Add_2","Add_3","City","Pin","State","Phone","Alternate_Name","Alternate_ph","ItemName"])
        parcels = list(queryset.values_list("CourierTag", "Consignee_Code","Consignee_Name", "WaybillNumber", "PickupDate", "Courier__courier_name", "Status","Add_1","Add_2","Add_3","City","Pin","State","Phone","Alternate_Name","Alternate_ph","ItemName"))
        parcels = [[field.encode('utf-8').strip() if type(field)==type("") else field for field in parcel] for parcel in parcels]
        for parcel in parcels:
            writer.writerow(parcel)

        return response

class ParcelInlineAddAdmin(admin.TabularInline):
    model = Parcel
    fields = ["WaybillNumber"]
    def get_queryset(self, request):
        qs = super(ParcelInlineAddAdmin, self).get_queryset(request)
        return qs.none()

class ParcelInlineAdmin(admin.TabularInline):
    model = Parcel
    readonly_fields = ["WaybillNumber", "Delivered", "PickupDate", "Status", "StatusCode", "Recepient", "From", "To", "DeliveryTime", "TrackingUrl" ,"AddedToShipway"]
    extra = 0

class CourierAdmin(admin.ModelAdmin):
    inlines = [ParcelInlineAddAdmin, ParcelInlineAdmin]
    search_fields = ('courier_name', )
    ordering = ('id',)
    readonly_fields = ("id", "courier_name", "image")


class ExcelFileAdmin(admin.ModelAdmin):
    list_display = ['fileLink']
    readonly_fields = ['fileLink']

admin.site.register(Parcel, ParcelAdmin)
admin.site.register(Courier, CourierAdmin)
admin.site.register(TrackingDetails)
admin.site.register(ExcelFile, ExcelFileAdmin)



#####
from django.contrib.admin.models import LogEntry, DELETION
from django.utils.html import escape
from django.core.urlresolvers import reverse


class LogEntryAdmin(admin.ModelAdmin):

    date_hierarchy = 'action_time'

    readonly_fields = []#LogEntry._meta.get_all_field_names()

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]


    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
        'change_message',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'
    
    def queryset(self, request):
        return super(LogEntryAdmin, self).queryset(request) \
            .prefetch_related('content_type')


admin.site.register(LogEntry, LogEntryAdmin)
#####