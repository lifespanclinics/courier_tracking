# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import urllib2, json
import requests
from django.db.models.signals import post_save
from django.dispatch import receiver
import pandas as pd
import difflib
from threading import Thread
import uuid
from django.conf import settings
import os
import csv

# Create your models here.

class Courier(models.Model):
    id = models.IntegerField(primary_key = True)
    courier_name = models.CharField(max_length = 200)
    image = models.CharField(max_length = 2000)
    def __unicode__(self):
        return self.courier_name

class Parcel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    AddedToShipway = models.BooleanField(default = False)
    WaybillNumber = models.CharField(max_length = 200)
    Courier = models.ForeignKey(Courier)
    Delivered = models.BooleanField(default = False)
    PickupDate = models.DateTimeField('Date', blank=True, null=True)
    Status = models.CharField(max_length = 200, blank=True, null=True)
    StatusCode = models.CharField(max_length = 200, blank=True, null=True)
    Recepient = models.CharField(max_length = 2000, blank=True, null=True)
    From = models.CharField(max_length = 200, blank=True, null=True)
    To = models.CharField(max_length = 200, blank=True, null=True)
    DeliveryTime = models.DateTimeField('DeliveryDate', blank=True, null=True)
    TrackingUrl = models.URLField(blank=True, null=True)

    CourierTag  = models.CharField(blank=True, null=True, max_length = 2000)
    Consignee_Code  = models.CharField(blank=True, null=True, max_length = 2000)
    Consignee_Name  = models.CharField(blank=True, null=True, max_length = 2000)
    CourierDate = models.CharField(blank=True, null=True, max_length = 2000)
    Add_1  = models.CharField(blank=True, null=True, max_length = 2000)
    Add_2  = models.CharField(blank=True, null=True, max_length = 2000)
    Add_3  = models.CharField(blank=True, null=True, max_length = 2000)
    City  = models.CharField(blank=True, null=True, max_length = 2000)
    Pin  = models.CharField(blank=True, null=True, max_length = 2000)
    State  = models.CharField(blank=True, null=True, max_length = 2000)
    Phone  = models.CharField(blank=True, null=True, max_length = 2000)
    Alternate_Name  = models.CharField(blank=True, null=True, max_length = 2000)
    Alternate_ph  = models.CharField(blank=True, null=True, max_length = 2000)
    ItemName  = models.CharField(blank=True, null=True, max_length = 2000)
    def __unicode__(self):
        return unicode(self.WaybillNumber)+" "+unicode(self.Courier.courier_name)

class TrackingDetails(models.Model):
    TimeRun = models.DateTimeField(auto_now=True)
    WaybillNumber = models.CharField(max_length = 200)
    Parcel = models.ForeignKey(Parcel)
    Courier = models.ForeignKey(Courier)
    PickupDate = models.DateTimeField('Date', blank=True, null=True)
    Status = models.CharField(max_length = 200, blank=True, null=True)
    StatusCode = models.CharField(max_length = 200, blank=True, null=True)
    Recepient = models.CharField(max_length = 2000, blank=True, null=True)
    From = models.CharField(max_length = 200, blank=True, null=True)
    To = models.CharField(max_length = 200, blank=True, null=True)
    DeliveryTime = models.DateTimeField('DeliveryDate', blank=True, null=True)
    TrackingUrl = models.URLField(blank=True, null=True)
    def __unicode__(self):
        return self.WaybillNumber + " " +self.Courier.courier_name+ " "+unicode(self.TimeRun.strftime('%Y-%m-%d %H:%M:%S'))

class ExcelFile(models.Model):
    File = models.FileField('File',upload_to='./')
    Date = models.DateTimeField(auto_now = True)
    def fileLink(self):
        if self.File:
            return '<a href="' + str(self.File.url) + '">' +str(self.File.name)+ str(self.File.path)+ '</a>'
        else:
            return '<a href="''"></a>'

    fileLink.allow_tags = True
    fileLink.short_description = "File Link"

    def save(self, *args, **kwargs):
        courier_names = list(Courier.objects.all().values_list("courier_name", flat = True))
        if (self.File.file.name).endswith(".xlsx"):
            xls = pd.ExcelFile(self.File.file)
            sheetX = xls.parse(0)
            cols = sheetX.columns.values.tolist()
            courier_matrix = sheetX.as_matrix()

        elif (self.File.file.name).endswith(".csv"):
            courier_matrix = []
            spamreader = csv.reader(self.File.file, delimiter=str(','), quotechar=str('"'))
            for row in spamreader:
                courier_matrix.append(row)
            courier_matrix=courier_matrix[1:]

        for i in courier_matrix:
            courier_name = difflib.get_close_matches(i[5], courier_names)[0]

            c = Courier.objects.get(courier_name = courier_name)
            
            defaults = {"CourierTag": i[0] , "Consignee_Code":i[1], "Consignee_Name":i[2], "CourierDate":i[4], "Add_1":i[7], "Add_2":i[8], "Add_3":i[9], "City":i[10], "Pin":i[11], "State":i[12], "Phone":i[13], "Alternate_Name":i[14], "Alternate_ph":i[15], "ItemName" :i[16]}
            print defaults
            for j in defaults:
                defaults[j] = unicode(defaults[j]).encode('ascii', 'ignore').decode('ascii')
            p, created = Parcel.objects.get_or_create(WaybillNumber = i[3], Courier = c, defaults = defaults)
                
        super(ExcelFile, self).save(*args, **kwargs)
        
def add_to_shipway(instance):
    data = {"username":"sarthakjain", "password":"d0ef2a757e6e0c05e6cfaec766bbfd45", "carrier_id":str(instance.Courier_id), "awb":instance.WaybillNumber, "order_id":str(instance.id), "email":"NA","phone":"NA","first_name":"NA","last_name":"NA","products":"NA","Company":"Oxygen"}
    data = json.dumps(data)

    url = "https://shipway.in/api/pushOrderData"

    headers = {
        'content-type': "application/json",
    }

    response = requests.request("POST", url, data=data, headers=headers)
    if response.status_code == 200:
       instance.AddedToShipway = True
       instance.save()    